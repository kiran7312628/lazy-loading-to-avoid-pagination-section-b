let productsdiv = document.getElementById('productsdiv');

let skip = 0;
async function getdata () {
try {
    document.getElementsByClassName('loader')[0].computedStyleMap.display="block";
  let response = await fetch(`https://dummyjson.com/products?limit=5&skip=${skip}`);
  let result = await response.json();
  console.log(result);
  skip=skip+5;
  document.getElementsByClassName('loader')[0].computedStyleMap.display="none";
  result.products.forEach((elem)=>{
    let id=elem.id;
    let title = elem.title;
    let div = document.createElement('div');
    div.innerHTML=` <p>${id}.${title}</p>
    <div class="imagediv">
        <img src=${elem.images[0]} alt=${id}>
    </div>`
    productsdiv.append(div);
  })
  
}
catch (error) {
    document.getElementsByClassName('loader')[0].computedStyleMap.display="none";
    console.log("This is the error",error)
}
}


//Throttling function used to prevent unnecessary multiple calls when reached end

const magicthrottle = (func,delay)=>{
    let lastfuncExtime = 0;
    return (...args)=>{
      let currTime = new Date().getTime();
      if(currTime-lastfuncExtime<delay)
      {
        return;
      }
      else {
        lastfuncExtime=currTime;
        document.getElementsByClassName('loader')[0].computedStyleMap.display="block";
        return setTimeout(()=>{func(...args)},1500); //calling the function sent here getdata()
      }
    }
}

const throttlefunc = magicthrottle (getdata,1500);


function myFunction (event) {

    let bodydiv = document.getElementById('bodydiv');
    console.log(document.documentElement.scrollTop,document.documentElement.clientHeight,document.documentElement.scrollHeight)
    if(document.documentElement.scrollTop + document.documentElement.clientHeight >= document.documentElement.scrollHeight-5)
    {
        console.log("data called");
        throttlefunc();
    }
}


getdata();  //Initial call
getdata();  //second call to fill the screen

